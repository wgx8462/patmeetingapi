package com.gb.petmeetingapi.controller;

import com.gb.petmeetingapi.model.PetMeeting.PetMeetingCreateRequest;
import com.gb.petmeetingapi.model.PetMeeting.PetMeetingItem;
import com.gb.petmeetingapi.service.PetMeetingService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pet-meeting")
public class PetMeetingController {
    private final PetMeetingService petMeetingService;

    @PostMapping("/join")
    public String setPetMeeting(@RequestBody PetMeetingCreateRequest request) {
        petMeetingService.setPetMeeting(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<PetMeetingItem> getPetMeetings() {
        return petMeetingService.getPetMeetings();
    }
}
