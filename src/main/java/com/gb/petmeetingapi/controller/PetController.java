package com.gb.petmeetingapi.controller;

import com.gb.petmeetingapi.entity.PetMeeting;
import com.gb.petmeetingapi.model.Pet.PetCreateRequest;
import com.gb.petmeetingapi.service.PetMeetingService;
import com.gb.petmeetingapi.service.PetService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pet")
public class PetController {
    private final PetMeetingService petMeetingService;
    private final PetService petService;

    @PostMapping("/new/pet-id/{petMeetingId}")
    public String setPet(@PathVariable long petMeetingId, @RequestBody PetCreateRequest request) {
        PetMeeting petMeeting = petMeetingService.getData(petMeetingId);
        petService.setPet(petMeeting, request);

        return "OK";
    }
}
