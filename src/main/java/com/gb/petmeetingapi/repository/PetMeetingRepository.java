package com.gb.petmeetingapi.repository;

import com.gb.petmeetingapi.entity.PetMeeting;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetMeetingRepository extends JpaRepository<PetMeeting, Long> {
}
