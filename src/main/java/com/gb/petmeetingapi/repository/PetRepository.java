package com.gb.petmeetingapi.repository;

import com.gb.petmeetingapi.entity.Pet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetRepository extends JpaRepository<Pet, Long> {
}
