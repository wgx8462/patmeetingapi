package com.gb.petmeetingapi.model.PetMeeting;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetMeetingItem {
    private String name;
    private String phoneNumber;
}
