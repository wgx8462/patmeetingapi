package com.gb.petmeetingapi.model.PetMeeting;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetMeetingCreateRequest {
        private String name;
        private String phoneNumber;
}
