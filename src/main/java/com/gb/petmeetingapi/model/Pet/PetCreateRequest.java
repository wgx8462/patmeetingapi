package com.gb.petmeetingapi.model.Pet;

import com.gb.petmeetingapi.enums.PetKind;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetCreateRequest {
    @Enumerated(value = EnumType.STRING)
    private PetKind petKind;
    private String petName;
}
