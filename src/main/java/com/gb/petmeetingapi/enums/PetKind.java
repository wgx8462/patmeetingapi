package com.gb.petmeetingapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PetKind {
    DOG("개"),
    CAT("고양이"),
    RABBIT("토끼"),
    BIRD("새"),
    HAMSTER("햄스터");

    private String name;
}
