package com.gb.petmeetingapi.service;

import com.gb.petmeetingapi.entity.Pet;
import com.gb.petmeetingapi.entity.PetMeeting;
import com.gb.petmeetingapi.model.Pet.PetCreateRequest;
import com.gb.petmeetingapi.repository.PetRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PetService {
    private final PetRepository petRepository;

    public void setPet(PetMeeting petMeeting, PetCreateRequest request) {
        Pet addData = new Pet();
        addData.setPetMeeting(petMeeting);
        addData.setPetKind(request.getPetKind());
        addData.setPetName(request.getPetName());
        petRepository.save(addData);
    }
}
