package com.gb.petmeetingapi.service;

import com.gb.petmeetingapi.entity.PetMeeting;
import com.gb.petmeetingapi.model.PetMeeting.PetMeetingCreateRequest;
import com.gb.petmeetingapi.model.PetMeeting.PetMeetingItem;
import com.gb.petmeetingapi.repository.PetMeetingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PetMeetingService {
    private final PetMeetingRepository petMeetingRepository;

    public PetMeeting getData(long id) {
        return petMeetingRepository.findById(id).orElseThrow();
    }

    public void setPetMeeting(PetMeetingCreateRequest request) {
        PetMeeting addData = new PetMeeting();
        addData.setName(request.getName());
        addData.setPhoneNumber(request.getPhoneNumber());
        petMeetingRepository.save(addData);
    }

    public List<PetMeetingItem> getPetMeetings() {
        List<PetMeeting> originList = petMeetingRepository.findAll();
        List<PetMeetingItem> result = new LinkedList<>();

        for (PetMeeting petMeeting : originList) {
            PetMeetingItem addItem = new PetMeetingItem();
            addItem.setName(petMeeting.getName());
            addItem.setPhoneNumber(petMeeting.getPhoneNumber());
            result.add(addItem);
        }
        return result;
    }
}
