package com.gb.petmeetingapi.entity;

import com.gb.petmeetingapi.enums.PetKind;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Pet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "petMeetingId", nullable = false)
    private PetMeeting petMeeting;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private PetKind petKind;

    @Column(nullable = false, length = 20)
    private String petName;
}
